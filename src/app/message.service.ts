import { Injectable } from '@angular/core';
import { Message, MESSAGES} from './message/message';

@Injectable()
export class MessageService {

  constructor() { }

  getMessages(): Message[] {
    return MESSAGES;
  }

}
