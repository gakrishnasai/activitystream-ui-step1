export class Message {
    name: string;
    message: string;
}

export const MESSAGES = [
    {name: 'Ram', message: 'Hey'},
    {name: 'Sita', message: 'Hello, when is the assignment due'},
    {name: 'Shyam', message: 'what assignment??'},
    {name: 'Gita', message: 'lol! It is due tomorrow sita' },
]