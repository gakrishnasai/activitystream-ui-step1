export class Circle {
    id: string;
    name: string;
}

export const CIRCLES = [
    {id: '10', name: 'Development'},
    {id: '11', name: 'HR'},
    {id: '12', name: 'QA'},
    {id: '13', name: 'Java'},
    {id: '14', name: 'Angular'},
    {id: '15', name: 'Database'},
    {id: '16', name: 'DevOps'},
]