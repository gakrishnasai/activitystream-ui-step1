import { Component, OnInit } from '@angular/core';
import { Circle } from './circle';
import { CircleService } from '../circle.service';


@Component({
  selector: 'app-circle',
  templateUrl: './circle.component.html',
  styleUrls: ['./circle.component.css']
})
export class CircleComponent implements OnInit {

  circles: Circle[];

  constructor(private circleService : CircleService) { }

  ngOnInit() {
    this.circles=this.circleService.getCircles();
  }

}
