export class User {
    id: string;
    name: string;
    password: string;
}

export const USERS = [
    {id: '101',name: 'inky', password: 'inky'},
    {id: '102',name: 'pinky', password: 'pinky'},
    {id: '103',name: 'ponky', password: 'ponky'},
    {id: '104',name: 'ram', password: 'ram'},
    {id: '105',name: 'shyam', password: 'shyam'},
    {id: '106',name: 'sita', password: 'sita'},
    {id: '107',name: 'gita', password: 'gita'},
]