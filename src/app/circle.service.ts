import { Injectable } from '@angular/core';
import { Circle, CIRCLES } from './circle/circle';

@Injectable()
export class CircleService {

  constructor() { }

  getCircles(): Circle[]{
    return CIRCLES;
  }

}
